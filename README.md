![PyPI - Python Version](https://img.shields.io/pypi/pyversions/django.svg)
![](https://gitlab.com/pronaturaveracruz/enciclovida-script-python-qgis/raw/master/OS-Windows_macOS_Linux-green.svg)
# Enciclovida script Python QGIS-3

Script para descargar registros de especies de Enciclovida directo en QGIS-3.

*  Permite descargar como archivos temporales y directamente en QGIS, con solo escribir el nombre científico de la especie, sin tener que navegar por enciclovida
*  El archivo de salida se presenta con la tabla de atributos desanidada, a diferencia del kml descargable desde enciclovida que incluye toda la información en una celda
*  El archivo se carga como capa vectorial

![](qgis_enciclovida.png)

**Para instalar las librerías faltantes necesarias: cssselect y lxml**  

*Usuarios de Linux y Mac*
```
$ pip3 install lxml

$ pip3 install cssselect
```


*Usuarios de Windows*

Abrir OSGeo4W Shell como administrador (con click derecho) (usualmente está en C:/)
[screenshots para guía](https://umar-yusuf.blogspot.com/2018/12/install-third-party-python-modules-in.html)
```
python -m ensurepip --default-pip

py3_env

python -m pip install lxml

python -m pip install cssselect
```


**Copiar al editor de Python en QGIS-3**
*Escribir el nombre científico de la especie en "species"*
```
import urllib3, re, requests 
from PyQt5.QtCore import *
from qgis.core import *
from lxml.html import parse

especie = "species"
doc = parse('http://www.enciclovida.mx/busquedas/resultados?utf8=%%E2%%9C%%93&busqueda=basica&id=&nombre=%s&button=' % especie.replace(" ", "+")).getroot()
doc_node = doc.cssselect(".result-img-container > a:nth-child(1)") #lxml wirkt mit den selector man kann einfach greifen
cat_num = re.search(r'(\d+)', doc_node[0].get("href")).group() 

url = "http://enciclovida.mx/especies/%s/ejemplares-snib.json" % cat_num
resp = requests.get(url) 
data = resp.json()
#*****************+
# esto convierte al json en un data frame
# import json
# from pandas.io.json import json_normalize
# json_normalize(data['resultados'])
# para guardar en un csv
# json_normalize(data['resultados']).to_csv("nombre_archivo.csv")

vl = QgsVectorLayer("Point", especie, "memory")
pr = vl.dataProvider()
pr.addAttributes([QgsField("especievalidabusqueda", QVariant.String),
                    QgsField("determinador",  QVariant.String),
                    QgsField("coleccion",  QVariant.String),
                    QgsField("colector",  QVariant.String),
                    QgsField("ejemplarfosil",  QVariant.String),
                    QgsField("estadomapa",  QVariant.String),
                    QgsField("idejemplar",  QVariant.String),
                    QgsField("institucion",  QVariant.String),
                    QgsField("probablelocnodecampo",  QVariant.String),
                    QgsField("proyecto",  QVariant.String),
                    QgsField("region",  QVariant.String),
                    QgsField("urlproyecto",  QVariant.String),
                    QgsField("urlejemplar",  QVariant.String),
                    QgsField("fechacolecta", QVariant.String)])

vl.updateFields()


for point in data['resultados']:
    point_feature = QgsFeature()
    point1 = QgsPointXY(float(point['longitud']), float(point['latitud']))
    point_feature.setGeometry(QgsGeometry.fromPointXY(point1))
    point_feature.setAttributes([point["especievalidabusqueda"], point["determinador"], point["coleccion"],point["colector"],point["ejemplarfosil"],point["estadomapa"],
    point["idejemplar"], point["institucion"],point["probablelocnodecampo"],point["proyecto"],point["region"],point["urlproyecto"], point["urlejemplar"],
    point["fechacolecta"]])
    pr.addFeatures([point_feature])

vl.updateExtents()
QgsProject.instance().addMapLayer(vl)
```

